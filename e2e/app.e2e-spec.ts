import { FendenwebfrontPage } from './app.po';

describe('fendenwebfront App', () => {
  let page: FendenwebfrontPage;

  beforeEach(() => {
    page = new FendenwebfrontPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
