appFenden.run(['$rootScope','$http', function($rootScope,$http,$location) {
	
	$rootScope.loading = false;
	$rootScope.body = false;
	
	//Acciones en carga 
	$rootScope.$on('$routeChangeStart', function() {
		$rootScope.loading = true;
		$rootScope.body = false;
	});

	$rootScope.$on('$viewContentLoaded', function() {
		$rootScope.loading = false;
		$rootScope.body = true;
	});

	$rootScope.$on('$routeChangeError', function() {
		console.log('Error routing');
	});
	
	
	
	//Generar lista de categorias
	$rootScope.listCategories = new Array;
	$http({
      method  : 'GET',
      url     : '/listCategories'
	}).then(function successCallback(response) {
		var data = response.data.data;
     	for (var i=0; i < data.length; i++) {
			$rootScope.listCategories.push({
				id : data[i]._id,
				path_categories : data[i].path_categories,
				category : data[i].category,
				classes : data[i].classes
			});
		}
	});
	
	$rootScope.like = function(type,post_id){
		$http({
	      method  : 'POST',
	      url     : '/like',
	      data : { 'type' : type, 'post_id' : post_id }
		}).then(function successCallback(response) {
			var data = response.data;
			if(data.success){
				var botonLike = angular.element('#like-'+post_id+' i');
				var botonDislike = angular.element('#dislike-'+post_id+' i');
				
				var countLike = parseInt(angular.element('#countlike-'+post_id).html());
				var countDislike = parseInt(angular.element('#countdislike-'+post_id).html());
				
				if(type == 'like'){
					botonLike.addClass('blue');
					botonDislike.removeClass('blue');
					angular.element('#countlike-'+post_id).html(countLike+1);

					if(countDislike != 0){
						if(botonLike.hasClass('blue'))
							angular.element('#countdislike-'+post_id).html(countDislike-1);
					}
				} else {
					botonDislike.addClass('blue');
					botonLike.removeClass('blue');
					angular.element('#countdislike-'+post_id).html(countDislike+1);
					
					if(countLike !=  0){
						if(botonDislike.hasClass('blue'))
							angular.element('#countlike-'+post_id).html(countLike-1);
					}
				}
			} else {
				
			}
		});
	};
    
    //Ultimos mensajes
	$http({
      method  : 'POST',
      url     : '/lastMessages'
	}).then(function successCallback(response) {
		var data = response.data;
     	if(data.success){
     		console.log(data.data);
     		$rootScope.listLastMessages = data.data;
     	}
	});
	

    
    $rootScope.logout = function(){
    	window.location.href='/logout';
    };
}]);
