import { Inject, Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class ProfileService {

  constructor(public http: Http) { }
  headers: any = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });
  getProfileData(id: string): any {
    return this.http.post('https://fendenapp.com/profileUserInfo', { id: id }, this.options).map(res => res.json());
  }

  getCommentsData(id: string): any {
    return this.http.post('https://fendenapp.com/listCommentsProfile', { id: id }, this.options).map(res => res.json());
  }

  addComment(id: string, commentary: string, image: string) {
    return this.http.post('https://fendenapp.com/addCommentProfile', { id: id, commentary: commentary, image: image }, this.options).map(res => res.json());
  }

  sendComplaint(id: string, textComplaint : string): any {
    return this.http.post('https://fendenapp.com/sendComplaint', { 'id' : id, 'complaint' : textComplaint }, this.options).map(res => res.json());
  }
  
  sendMessage(id: string, textMessage : string): any {
    return this.http.post('https://fendenapp.com/sendMessage', { 'id' : id, 'message' : textMessage }, this.options).map(res => res.json());
  }
}
