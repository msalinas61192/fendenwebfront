import { Component, NgModule, HostBinding, ViewChild, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgIf } from '@angular/common';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'], 
  providers: [ProfileService] 
})
export class ProfileComponent implements OnInit {

  profileData: {};
  comments: Array<{ id: string, avatar: string, name: string, image: string, commentary: string, date: string }> = [];
  textComment: any;
  textMessage: string;
  textComplaint: string;
  idProfile: any;
  showProfile: boolean = false;
  showAlert: boolean = false;
  resultAlert: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
              private profileService: ProfileService,
              private renderer: Renderer2,
              private elementRef: ElementRef) {
      this.activatedRoute.params.subscribe((params: Params) => {
      this.idProfile = params['id'];
      this.profileService.getProfileData(this.idProfile).subscribe(res => {
        this.profileData = {
          date : res.data.date,
          name : ( res.data.first_name != undefined ? res.data.first_name : '' )+( res.data.last_name != undefined ? ' '+res.data.last_name : '' ),
          mail : res.data.mail,
          profile_photo : res.data.profile_photo,
          description : res.data.description
        };
        this.showProfile = true;
      });

      this.profileService.getCommentsData(params['id']).subscribe(res => {
        for (let i = 0; i < res.data.length; i++) {
          let arr_date = res.data[i].date.split('.');
          this.comments.push({
            id: res.data[i].id,
            avatar: res.data[i].postedBy.profile_photo,
            name: res.data[i].postedBy.first_name + ((res.data[i].postedBy.last_name != undefined) ? ' ' + res.data[i].postedBy.last_name : ''),
            image: (res.data[i].image != undefined ? res.data[i].image : false),
            commentary: res.data[i].commentary,
            date: arr_date[0].replace('T', ' ')
          });
        }
      });
      

    });
  }

  addComment(){
    this.profileService.addComment(this.idProfile,this.textComment,'').subscribe(res => {
      this.comments.unshift({
        id: res.data.id,
        avatar: res.data.postedBy.profile_photo,
        name: res.data.postedBy.first_name + ((res.data.postedBy.last_name != undefined) ? ' ' + res.data.postedBy.last_name : ''),
        commentary: res.data.commentary,
        date: '',
        image: ''
      });
      console.log(this.comments);
    });
  }

  showComments() { 
    if (this.comments.length > 0) {
      return true;
    } else { 
      return false;
    }
  }

  sendComplaint(){
    this.profileService.sendComplaint(this.idProfile, this.textComplaint).subscribe(res => {
      console.log(res);
      if(res.success){
        this.resultAlert = true;
      } else {
        this.resultAlert = false;
      }
      this.showAlert = true;
    });
  }
  

  sendMessage(){
    this.profileService.sendMessage(this.idProfile, this.textMessage).subscribe(res => {
      console.log(res);
      if(res.success){
        this.resultAlert = true;
      } else {
        this.resultAlert = false;
      }
      this.showAlert = true;
    });
  }
  

  hidenAlert(){
    this.showAlert = false;
    this.resultAlert = false;
  }

  ngOnInit() {
  }

}
