import { Component, NgModule, HostBinding, ViewChild, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//Services
import { OfferService } from './offer.service';
import { WindowRef } from '../WindowRef';

declare var google;
declare var blueimp;

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css'], 
  providers: [OfferService,WindowRef] 
})
export class OfferComponent implements OnInit {
  offerId : any;
  offerData : any;
  listComments : any;
  shortListComments : any;
  location = {};  
  me = '/assets/img/maps/me-icon.png';
  textComment: any;

  icons: {};
  infowindow: any;
  origin: any;
  end: any;
  placeMarker: any;
  directionsService: any;
  directionsDisplay: any;

  @ViewChild('map') map: ElementRef;
  @ViewChild('links') links: ElementRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private offerService: OfferService,
    private renderer:Renderer2, 
    private elementRef:ElementRef,
    private winRef: WindowRef) { 

    this.activatedRoute.params.subscribe((params: Params) => {
      this.offerId = params['id'];
    });

    this.offerService.getOfferData(this.offerId).subscribe(res => {
      this.offerData = res.data[0];
    });

    this.offerService.getComments(this.offerId).subscribe(res => {
      this.listComments = res.data;
      this.shortListComments = res.data.slice(0,5);
    });

    let iconBase = 'assets/img/maps/';
    this.icons = {
      parking: { icon: iconBase + 'blue-icon.png' },
      library: { icon: iconBase + 'green-icon.png' },
      info: { icon: iconBase + 'blue-icon.png' },
      me: {  icon: iconBase + 'me-icon.png' }
    };
  }

  addComment(){
    this.offerService.addComment(this.offerId, this.textComment).subscribe(res => {
      if(res.success){
        this.listComments.unshift(res.data);
        this.shortListComments.unshift(res.data);
        this.shortListComments = this.shortListComments.slice(0,5);
      }
    });
  }
 

    
  ngOnInit() {
    setTimeout(() => {
      this.renderer.listen(this.links.nativeElement,'click', (event) => {
        event = event || this.winRef.nativeWindow.event;
        let target = event.target || event.srcElement;
        let link = target.src ? target.parentNode : target;
        let options = { index: link , event: event};
        let links = this.links.nativeElement.getElementsByTagName('a');
        console.log(links);
        blueimp.Gallery(links, options);
      });
    },400);

    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition((position) => {

          this.origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          this.end = new google.maps.LatLng(this.offerData.latitude, this.offerData.longitude);
          
          this.placeMarker = (loc: any,content: any,type: any, origin: any, end: any) => {
            let marker = new google.maps.Marker({
              position : loc,
              type : 'info',
              draggable: (type == 'me'?true:false),
              map      : this.map,
              animation: google.maps.Animation.DROP,
              icon: this.icons[type].icon,
            });

            google.maps.event.addListener(marker, 'dragend', (evt) => {
              this.directionsService.route({
                origin: new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng()),
                destination: end,
                travelMode: google.maps.TravelMode.WALKING
              }, (response, status) => {
                if(status == google.maps.DirectionsStatus.OK) {
                  this.directionsDisplay.setDirections(response);
                } else {
                  console.log('Directions request failed due to ' + status);
                }
              });
            });

            google.maps.event.addListener(marker, 'click', (evt) => {
              this.infowindow = new google.maps.InfoWindow();
              this.infowindow.close();
              this.infowindow.setContent(content);
              this.infowindow.open(this.map, marker);

              if(type != 'me'){
                this.directionsService = new google.
                maps.DirectionsService;
                if(this.directionsDisplay != undefined){
                  this.directionsDisplay.setMap(null);
                }
                this.directionsDisplay = new google.maps.DirectionsRenderer({
                  map: this.map
                });
                this.directionsDisplay.setOptions( { suppressMarkers: true } );

                this.directionsService.route({
                  origin: origin,
                  destination: end,
                  travelMode: google.maps.TravelMode.WALKING
                }, (response, status) => {
                  if(status == google.maps.DirectionsStatus.OK) {
                    this.directionsDisplay.setDirections(response);
                  } else {
                    console.log('Directions request failed due to ' + status);
                  }
                });
              }
            });
          }

          setTimeout(() => {
            this.map = new google.maps.Map(this.map.nativeElement, {
              center: this.origin,
              scrollwheel: false,
              zoom: 15
            });
            this.placeMarker(this.end,'Producto','info',this.origin,this.end);
	          this.placeMarker(this.origin,'Tú Ubicación','me',this.origin,this.end);
          }, 500);

        },(error) => {
          console.log(error);
        });
    }
  }

 
}
