import { Inject, Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class OfferService {

  constructor(public http: Http) { }

  getOfferData(id: string): any {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('https://fendenapp.com/offerData',{ 'id' : id }, options).map(res =>  res.json());  
  }

  getComments(id: string): any {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('https://fendenapp.com/listComments',{ 'id' : id }, options).map(res =>  res.json());  
  }
  
  addComment(id: any, comment: any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let data = { 
      'id' : id, 
      'commentary' : comment,
      /* 'image' : angular.element('#imageInputComment').val(),
      'thumb' : angular.element('#imageInputCommentThumb').val() */
	  };
    return this.http.post('https://fendenapp.com/addComment',data, options).map(res =>  res.json());  
  }

}
