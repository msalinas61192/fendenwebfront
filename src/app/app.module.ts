import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { OfferupComponent } from './offerup/offerup.component';
import { OfferComponent } from './offer/offer.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MessagesComponent } from './messages/messages.component';
import { NearbyComponent } from './nearby/nearby.component';
import { CategoryComponent } from './category/category.component';
import { SettingsComponent } from './settings/settings.component';

const appRoutes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'offer/:id', component: OfferComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'offerup', component: OfferupComponent },
  { path: 'messages', component: MessagesComponent },
  { path: 'nearby', component: NearbyComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'category/:name', component: CategoryComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    OfferupComponent,
    CategoryComponent,
    OfferComponent,
    ProfileComponent,
    DashboardComponent,
    MessagesComponent,
    NearbyComponent,
    CategoryComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ModalModule.forRoot(),
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
