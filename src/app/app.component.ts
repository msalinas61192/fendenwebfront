import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'], 
  providers: [AppService] 
})
export class AppComponent  implements OnInit {

  listCategories: any;
  constructor(private appService: AppService){
    this.appService.getCategories().subscribe(res => {
      this.listCategories = res.data;
      console.log(res.data);
    });
  }
  ngOnInit(): void {

  }
  title = 'app';


}
