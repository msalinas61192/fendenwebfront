import { TestBed, inject } from '@angular/core/testing';

import { NearbyService } from './nearby.service';

describe('NearbyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NearbyService]
    });
  });

  it('should be created', inject([NearbyService], (service: NearbyService) => {
    expect(service).toBeTruthy();
  }));
});
