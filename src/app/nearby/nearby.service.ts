import { Inject, Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class NearbyService {

  constructor(public http: Http) { }

  getOffersNearby(lat: any, lng: any): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('https://fendenapp.com/listOffersNearby',{ 'lat' : lat, 'lng' : lng }, options).map(res =>  res.json());  
  }
}
