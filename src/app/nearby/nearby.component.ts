import { Component, NgModule, HostBinding, ViewChild, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { NearbyService } from './nearby.service';

declare var google;
@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.component.html',
  styleUrls: ['./nearby.component.css'],
  providers: [NearbyService]
})
export class NearbyComponent implements OnInit {
  
  icons: {};
  infowindow: any;
  origin: any;
  end: any;
  placeMarker: any;
  directionsService: any;
  directionsDisplay: any;
  position: any;
  tempLocation: any;

  
  @ViewChild('map') map: ElementRef;
  constructor(
    private renderer: Renderer2, 
    private elementRef: ElementRef,
    private nearbyService: NearbyService) {


    
    let iconBase = 'assets/img/maps/';
    this.icons = {
      parking: { icon: iconBase + 'blue-icon.png' },
      library: { icon: iconBase + 'green-icon.png' },
      info: { icon: iconBase + 'blue-icon.png' },
      me: {  icon: iconBase + 'me-icon.png' }
    };
  }

  ngOnInit() {
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position) => {
          this.origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          
          this.placeMarker = (loc: any,content: any,type: any, origin: any, end: any) => {
            let marker = new google.maps.Marker({
              position : loc,
              type : 'info',
              draggable: (type == 'me'?true:false),
              map      : this.map,
              animation: google.maps.Animation.DROP,
              icon: this.icons[type].icon,
            });

            google.maps.event.addListener(marker, 'dragend', (evt) => {
              console.log('Se mueve');
              this.origin = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
              this.directionsService.route({
                origin: this.origin,
                destination: this.end,
                travelMode: google.maps.TravelMode.WALKING
              }, (response, status) => {
                if(status == google.maps.DirectionsStatus.OK) {
                  this.directionsDisplay.setDirections(response);
                } else {
                  console.log('Directions request failed due to ' + status);
                }
              });
            });

            google.maps.event.addListener(marker, 'click', (evt) => {
              this.infowindow = new google.maps.InfoWindow();
              this.infowindow.close();
              this.infowindow.setContent(content);
              this.infowindow.open(this.map, marker);

              if(type != 'me'){
                this.end = loc;
                this.directionsService = new google.maps.DirectionsService;
                if(this.directionsDisplay != undefined){
                  this.directionsDisplay.setMap(null);
                }
                this.directionsDisplay = new google.maps.DirectionsRenderer({
                  map: this.map
                });
                this.directionsDisplay.setOptions( { suppressMarkers: true } );

                this.directionsService.route({
                  origin: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                  destination: loc,
                  travelMode: google.maps.TravelMode.WALKING
                }, (response, status) => {
                  if(status == google.maps.DirectionsStatus.OK) {
                    this.directionsDisplay.setDirections(response);
                  } else {
                    console.log('Directions request failed due to ' + status);
                  }
                });
              }
            });
          }

          setTimeout(() => {
            this.map = new google.maps.Map(this.map.nativeElement, {
              center: this.origin,
              scrollwheel: false,
              zoom: 15
            });
            //this.placeMarker(this.end,'Procuto','info',this.origin,this.end);
            this.placeMarker(this.origin,'Tú Ubicación','me',this.origin,this.end);
                      

            this.nearbyService.getOffersNearby(position.coords.latitude, position.coords.longitude).subscribe(res => {
              res.forEach(element => {
                let content = '<img style="width: 70px; height: 70px; float: left; margin-right: 10px;" src="/offerImage/'+element._id+'/0">';
                content += element.title+', <a href="/offer/'+element._id+'">Leer mas</a>';
				        let latLng = new google.maps.LatLng(element.latitude, element.longitude );
				        this.placeMarker(latLng,content,'info',this.origin,this.end);
              });
            });
          }, 500);
      
      
      },(error) => {    
        console.log(error);
      });
    }
  }

}
