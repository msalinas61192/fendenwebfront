import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferupComponent } from './offerup.component';

describe('OfferupComponent', () => {
  let component: OfferupComponent;
  let fixture: ComponentFixture<OfferupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
