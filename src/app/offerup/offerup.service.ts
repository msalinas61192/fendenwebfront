import { Inject, Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class OfferupService {

  constructor(public http: Http){ }

  getOfferData(id: string): any {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('https://fendenapp.com/listCategories',{}, options).map(res =>  res.json());  
  }

  getLocation(direccion: string): any {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+direccion).map(res =>  res.json());  
  }

  getAdress(position: any): any {
    let myLatLng = {lat: position.coords.latitude, lng: position.coords.longitude};
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+myLatLng.lat+','+myLatLng.lng+'&sensor=false').map(res =>  res.json());  
  }

  upOfferData(data: any): any {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('https://fendenapp.com/offerup',data, options).map(res =>  res.json());  
  }


}