import { TestBed, inject } from '@angular/core/testing';

import { OfferupService } from './offerup.service';

describe('OfferupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OfferupService]
    });
  });

  it('should be created', inject([OfferupService], (service: OfferupService) => {
    expect(service).toBeTruthy();
  }));
});
