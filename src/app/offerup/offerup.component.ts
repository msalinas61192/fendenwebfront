import { Component, NgModule, HostBinding, ViewChild, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


import { AppService } from '../app.service';
import { OfferupService } from './offerup.service';

declare var jQuery:any;
declare var $:any;
declare var google;
declare var sweetAlert;
declare var markers;

@Component({
  selector: 'app-offerup',
  templateUrl: './offerup.component.html',
  styleUrls: ['./offerup.component.css'], 
  providers: [OfferupService]
})
export class OfferupComponent implements OnInit {
  @ViewChild('category') category: ElementRef;
  @ViewChild('map') mapElement: ElementRef;


  listImages: any[] = [];
  listImagesThumbs: any[] = [];
  map: any;
  datosFormulario: any;
  listCategories: any;
  position: any;
  placeMarker: any;
  marker: any;
  infowindow: any;
  step: number;
  message: any;
  posMap: any;
  iconBase = 'assets/img/maps/';
  icons = {
    parking: { icon: this.iconBase + 'blue-icon.png' },
    library: { icon: this.iconBase + 'green-icon.png' },
    info: { icon: this.iconBase + 'blue-icon.png' },
    me: {  icon: this.iconBase + 'me-icon.png' }
  };
  commentLenght: number = 0;

  constructor(private renderer: Renderer2, 
              private elementRef: ElementRef,
              private appService: AppService,
              private offerupService: OfferupService,
              private router: Router) {}
  

  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.onload = () => {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        
        if(img.width > 1000){
            canvas.width = img.width*(1000/img.width);
            canvas.height = img.height*(1000/img.width);
        } else {
            if(img.height > 1000){
                canvas.width = img.width*(1000/img.height);
                canvas.height = img.height*(1000/img.height);
            } else {
                canvas.height = img.height;
                canvas.width = img.width;
            }
        }
          
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
  
  
  convertToBase64Thumbs(url, outputFormat) {
      return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        
        canvas.width = 100;
        canvas.height = 100;
          
        ctx.drawImage(img, 0, 0, 100, 100);

        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
              
  changeStep(type: string){
    if(type == 'next'){
      this.step++;
    } else {
      this.step--;
    }
    let hideSteps = () => {
      $('.content-step').addClass('hide');
      $('#step-'+this.step).removeClass('hide');
    };


    switch (this.step) {
      case 1:
        hideSteps();
        $("#previousButton").attr("disabled", true);
        $("#nextButton").attr("disabled", false);
        $("#upButton").attr("disabled", true);
        $(".step-mark").removeClass('active');
        $("#step-mark-"+this.step).addClass('active');
        break;
      case 2:
        this.datosFormulario = {
            'ofertaTitulo' : $('#ofertaTitulo').val(),
            'ofertaPrecio' : $('#ofertaPrecio').val(),
            'category' : $('#category').val(),
            'ofertaTienda' : $('#ofertaTienda').val(),
            'ofertaSucursal' : $('#ofertaSucursal').val(),
            'ofertaInicio' : $('#ofertaInicio').val(),
            'ofertaFin' : $('#ofertaFin').val(),
            'ofertaComentario' : $('#ofertaComentario').val()
        };
        console.log(this.datosFormulario);
        this.message = '';
        if(this.datosFormulario.ofertaTitulo <= 8) this.message += '-El titulo de la oferta debe tener al menos de 8 caracteres\n';
        if(this.datosFormulario.category == '') this.message += '-Debe seleccionar una categoría\n'; 
        if(parseInt(this.datosFormulario.ofertaPrecio) < 0 || this.datosFormulario.ofertaPrecio == '') this.message += '-Debe ingresar el precio\n'; 
        if(this.datosFormulario.ofertaTienda.length <= 1) this.message += '-El nombre de la tienda debe tener al menos de 2 caracteres\n';
        if(this.datosFormulario.ofertaSucursal.length <= 3) this.message += '-El nombre de la sucursal debe tener al menos 4 caracteres\n';
        if(this.datosFormulario.ofertaInicio == null
          || this.datosFormulario.ofertaInicio == undefined
          || this.datosFormulario.ofertaInicio == ''
        ) this.message += '-Debe ingresar una fecha de inicio\n';
        
        if(this.datosFormulario.ofertaFin == null
          || this.datosFormulario.ofertaFin == undefined
          || this.datosFormulario.ofertaFin == ''
        ) this.message += '-Debe ingresar una fecha de fin\n';
        if(this.datosFormulario.ofertaComentario.length <=19) this.message += '-La descripción de la oferta es muy corta\n'; 


        if(this.message === ''){
          hideSteps();
          $("#previousButton").attr("disabled", false);
          $("#nextButton").attr("disabled", false);
          $("#upButton").attr("disabled", true);
          $(".step-mark").removeClass('active');
          $("#step-mark-"+this.step).addClass('active');
        } else {
          sweetAlert("Oops...",this.message, "error");
          this.step--;
        }
        break;
      case 3:
        this.message = '';
        if($("[name='images']")[0].files.length === 0)  this.message = '-Debe subir al menos una imagen de la oferta\n';
        if(this.message === ''){
          for (var i = 0; i < $("[name='images']")[0].files.length; i++) {
            let reader = new FileReader();

            let numberImage = i+1;
            reader.readAsDataURL($("[name='images']")[0].files[i]);
            reader.onload = () => {
              let imageBase64 = reader.result;
              let convertToBase64 = this.convertToBase64;
              let convertToBase64Thumbs = this.convertToBase64Thumbs;
              convertToBase64(imageBase64,'image/jpeg').then((data) => {
                this.datosFormulario['imagen'+numberImage] = data;
              });
              convertToBase64Thumbs(imageBase64,'image/jpeg').then((data) => {
                this.datosFormulario['thumb'+numberImage] = data;
              });
            };
          }

          hideSteps();
          $("#previousButton").attr("disabled", false);
          $("#nextButton").attr("disabled", true);
          $("#upButton").attr("disabled", false);
          $(".step-mark").removeClass('active');
          $("#step-mark-"+this.step).addClass('active');
          this.initMap();
        } else {
          sweetAlert("Oops...",this.message, "error");
          this.step--;
        }
        break;
      
      case 4:
        this.datosFormulario.latitude = this.position.latitude;
        this.datosFormulario.longitude = this.position.longitude;
        this.datosFormulario.ofertaUbicacion = $('#ofertaUbicacion').val();
        this.offerupService.upOfferData(this.datosFormulario).subscribe(res => {
          if(res.success){
            this.router.navigate(['/offer/'+res.id]);
          } else {
            sweetAlert("Oops...","Ocurrio un error al subir la oferta", "error");            
          }
        });

        console.log('Finish');
        console.log(this.datosFormulario);

      break;
    }
    
  }



  

  searchOffer(){
    let direcion = $('#ofertaUbicacion').val();
    this.offerupService.getLocation(direcion).subscribe(res => {
      this.position = { latitude : res.results[0].geometry.location.lat, longitude : res.results[0].geometry.location.lng }; 

      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        center: new google.maps.LatLng(this.position.latitude, this.position.longitude),
        scrollwheel: false,
        zoom: 15
      });
      this.placeMarker(new google.maps.LatLng(this.position.latitude, this.position.longitude),'Producto','info');
    });
  }

  initMap(){
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position) => {

        this.offerupService.getAdress(position).subscribe(res => {
          $('#ofertaUbicacion').val(res.results[0].formatted_address);
        });

        this.position = { latitude : position.coords.latitude, longitude : position.coords.longitude };
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            center: new google.maps.LatLng(this.position.latitude, this.position.longitude),
            scrollwheel: false,
            zoom: 15
        });
        this.placeMarker(new google.maps.LatLng(this.position.latitude,this.position.longitude),'Producto','info');

      });
    }
  }

  cambioComentario($event){
    this.commentLenght = $event.length;
  }

  ngOnInit() {
    this.step = 1;
    //$('#step-1').addClass('animated fadeOutUp');
    this.appService.getCategories().subscribe(res => {
      if(res.success){
        this.listCategories = res.data;
      }
    });

    this.placeMarker = (loc: any,content: any,type: any) => {
      this.marker = new google.maps.Marker({
        position : loc,
        type : 'info',
        draggable: (type == 'me'?true:false),
        map      : this.map,
        animation: google.maps.Animation.DROP,
        icon: this.icons[type].icon,
      });

      
      google.maps.event.addListener(this.marker, 'click', (evt) => {
        this.infowindow = new google.maps.InfoWindow();
        this.infowindow.close();
        this.infowindow.setContent(content);
        this.infowindow.open(this.map, this.marker);
      });
    }


    setTimeout(() => {
      //$('#step-1').addClass('animated fadeOutUp');
      $("#category").select2();
      $('.input-group.date').datepicker({
        todayBtn: "linked",
        format: 'yyyy-mm-dd',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
      });
      
      $("#images").fileinput({
        language: "es",
        allowedFileExtensions: ["jpg", "png"]
      });

    },1000);
  }

}