import { Component, OnInit, HostListener, Inject, Injectable, NgModule, HostBinding, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { CategoryService } from './category.service';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WindowRef } from "../WindowRef";


declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'], 
  providers: [CategoryService,WindowRef] 
})
export class CategoryComponent implements OnInit {
  listOffers: Array<any> = new Array();
  lastDate: any;
  idCategory: any;

  constructor(@Inject(DOCUMENT) private document: any,
    private categoryService: CategoryService,
    private renderer: Renderer2,
    private activatedRoute: ActivatedRoute,
    private elementRef: ElementRef) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.categoryService.getInfoCategory(params['name']).subscribe(category => {
        this.idCategory = category.data[0]._id;
        this.categoryService.listCategoryOffers(this.idCategory).subscribe(res => {
          for (let index = 0; index < res.data.length; index++) {
            this.listOffers.push(this.getItemOffer(res.data[index]));
          }
        });
        console.log(this.listOffers);
      });
    });
  }

  getItemOffer(data) {
    console.log(data);
    let nombre_usuario = '';
    if (data.postedBy != null) {
      nombre_usuario = data.postedBy.first_name + ' ' + (data.postedBy.last_name != undefined ? data.postedBy.last_name : '');
    }
    let url = 'https://www.facebook.com/sharer.php?caption=' + data.title + '&description=' + data.commentary + '&u=https://fendenapp.com/more/' + data.id + '&picture=' + (data.thumbnail != undefined ? 'https://fendenapp.com/offerImage/' + data.id + '/0' : '');
    let response: any = {
      id: data.id,
      title: (data.title.length > 30 ? data.title.substring(0, 30) + '...' : data.title),
      price: this.transform(data.price),
      shop: data.shop,
      branch: data.branch,
      start_date: String(data.start_date).substring(0, 10),
      end_date: String(data.end_date).substring(0, 10),
      commentary: (data.commentary.length > 190 ? data.commentary.substring(0, 190) + '...' : data.commentary),
      address: data.address,
      image: '/offerImage/'+data.id+'/0/thumb',
      avatar: (data.postedBy != null ? data.postedBy.profile_photo : '/assets/img/generic-avatar.png'),
      user_id: (data.postedBy != null ? data.postedBy._id : ''),
      name: nombre_usuario,
      category: data.category,
      share_facebook: () => { this._window(url); },
      share_twitter: 'https://twitter.com/intent/tweet?text=' + data.title + '&url=https://fendenapp.com/more/' + data.id,
      share_google: 'https://plus.google.com/share?url=https://fendenapp.com/more/' + data.id,
      likes: data.likes,
      dislikes: data.dislikes,
      likes_count: data.likes_count,
      dislikes_count: data.dislikes_count
    };
    this.lastDate = data.date;
    return response;
  }


  transform(value: number): any {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  _window(url: string){
      console.log(url);
      window.open(url,"popup", "width=300,height=200,left=10,top=150");         
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    if ((window.innerHeight + window.scrollY) >= this.document.body.scrollHeight) {
      let filter = $("#textFilter").val();
      this.categoryService.loadMore(this.idCategory, this.lastDate,'home',filter).subscribe(res => {
        res.data.forEach(item => {
          this.listOffers.push(this.getItemOffer(item));
        });
      });
    }
  }

}
