import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoryService {
  headers: any = new Headers({ 'Content-Type': 'application/json' });
  options: any = new RequestOptions({ headers: this.headers });
  constructor(public http: Http) { }


  getInfoCategory(nombre: String): any {
    return this.http.post('https://fendenapp.com/infoCategory',{ 'nombre' : nombre }, this.options).map(res =>  res.json());  
  }

  listCategoryOffers(id: any){
    return this.http.post('https://fendenapp.com/listCategoryOffers',{ 'id' : id }, this.options).map(res =>  res.json());  
  }

  loadMore(category: string,date: any, type: string, filter: string = ''): any {
    let params = { 'date' : date, 'type' : type, 'category' : category };
    if(filter.length > 0){
      params['filter'] = filter;
    }
    return this.http.post('https://fendenapp.com/nextOffer',params, this.options).map(res =>  res.json());  
  }

}
