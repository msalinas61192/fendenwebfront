import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {
  headers: any = new Headers({ 'Content-Type': 'application/json' });
  options: any = new RequestOptions({ headers: this.headers });
  constructor(public http: Http) {}

  getListOffers(filter: string = ''): any {
    let params = {};
    if(filter.length > 0){
      params['filter'] = filter;
    }
    return this.http.post('https://fendenapp.com/listOffers',params, this.options).map(res =>  res.json());  
  }

  loadMore(date: any, type: string, filter: string = ''): any {
    let params = { 'date' : date, 'type' : type };
    if(filter.length > 0){
      params['filter'] = filter;
    }
    return this.http.post('https://fendenapp.com/nextOffer',params, this.options).map(res =>  res.json());  
  }

}
