import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {
  constructor(public http: Http) {}

  getCategories(){
    return this.http.get('https://fendenapp.com/listCategories').map((res:Response) => res.json());
  }


}
